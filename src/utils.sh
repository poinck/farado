#!/bin/bash

# check config var
# - set to a default, if not set
check_config() {
    var="$1"
    value="$2"
    show_default_warning=1

    if [[ -z "${value}" ]] ; then
        if [[ "${var}" == "garmin_mount" ]] ; then
            value="/run/media/${USER}/GARMIN/GARMIN/"
        elif [[ "${var}" == "garmin_data" ]] ; then
            value="${farado_dir}_data/garmin/"
        elif [[ "${var}" == "farado_data" ]] ; then
            value="${farado_dir}_data/farado/"
        elif [[ "${var}" == "fit_lib_dir" ]] ; then
            value="${farado_dir}lib/garmin-fit/"
            echo "info: using modified bundled version of garmin-fit (perl)"
            show_default_aring=0
        fi
        if [[ "${show_default_warning}" -eq 1 ]] ; then
            echo "warning: ${var} was set to default '${value}'"
        fi
    fi

    eval "${var}=${value}"
}
