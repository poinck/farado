#!/bin/bash
# - copy using rsync

if [[ -f ~/.faradorc ]] ; then
    . ~/.faradorc
    . ${farado_dir}src/utils.sh
else
    echo "error: farado has no default config, please define at least farado_dir in ~/.faradorc"
    exit 1
fi

check_config "garmin_mount" "${garmin_mount}"
check_config "garmin_data" "${garmin_data}"

if [[ ! -f "${garmin_mount}/GarminDevice.xml" ]] ; then
    echo "error: your garmin device is not mounted at ${garmin_mount}"
    exit 2
fi

if [[ ! -d "${garmin_data}" ]] ; then
    mkdir -p ${garmin_data}
    echo "info: missing ${garmin_data} dir was created"
fi

# todo
# - copy using rsync -ai
# - exclude uneeded directories
today="$( date --utc --rfc-3339='date' )"
mkdir -p ${garmin_data}${today}
rsync -ai \
    -f'- TEXT/' \
    -f'- CPYRIGHT.TXT' \
    -f'- APPS/' \
    -f'- Adjstmts/' \
    -f'- ChngLog/' \
    -f'- Debug/' \
    -f'- EVNTLOGS/' \
    -f'- EXPRESS/' \
    -f'- EXTDATA/' \
    -f'- HSA/' \
    -f'- NewFiles/' \
    -f'- PaceBands/' \
    -f'- REMOTESW/' \
    -f'- TLG/' \
    -f'- TempFIT/' \
    ${garmin_mount}* ${garmin_data}${today}/.
rc=$?
if [[ "${rc}" -eq 0 ]] ; then
    echo "newest garmin data from ${today} collected."
fi

