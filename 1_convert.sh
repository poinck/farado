#!/bin/bash
# - convert to JSON using garmin-fit
# - $1: date in RFC-3339, garmin data dir collection (eg. ~/data/garmin/2024-02-17)

if [[ -f ~/.faradorc ]] ; then
    . ~/.faradorc
    . ${farado_dir}src/utils.sh
else
    echo "error: farado has no default config, please define at least farado_dir in ~/.faradorc"
    exit 1
fi

check_config "garmin_data" "${garmin_data}"
check_config "farado_data" "${farado_data}"
check_config "fit_lib_dir" "${fit_lib_dir}"

if [[ ! -d "${farado_data}" ]] ; then
    mkdir -p ${farado_data}
    echo "info: missing ${farado_data} dir was created"
fi

# todo
# - convert to JSON using garmin-fit
today="$( date --utc --rfc-3339='date' )"
garmin_collection="$1"
if [[ -z "${garmin_collection}" ]] ; then
    garmin_collection="${today}"
fi

if [[ ! -d "${garmin_data}${garmin_collection}" ]] ; then
    echo "error: there is no garmin data from ${garmin_collection}, run 0_collect.sh first"
    exit 3
fi

convert_fit() {
    pattern="$1"
    category="$2"

    for f in ${garmin_data}${garmin_collection}/${pattern} ; do
        nf="${f##*/}"
        echo -en "\r                            "
        echo -en "\r${category}: ${nf}"
        if [[ -f "${f}" ]] ; then
            mkdir -p ${farado_data}${garmin_collection}/${category}
            ${fit_lib_dir}fitdump.pl -print_json=1 -numeric_date_time=0 ${f} > ${farado_data}${garmin_collection}/${category}/${nf}.json
        fi
    done
}

echo "converting files in ${garmin_data}${garmin_collection}"
convert_fit "Monitor/*.FIT" "monitor"
convert_fit "Sleep/*.FIT" "sleep"
convert_fit "Activity/*.fit" "activity"
convert_fit "Totals/*.fit" "totals"
echo -en "\rgarmin data from ${garmin_collection} converted.\n"

