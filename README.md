# Readme: farado

Farado (esperanto for activity) is a toolbox to collect, convert, import and graph your activities gathered by a Garmin watch using the Garmin-FIT Perl library and R.

The toolbox was tested with data from a Forerunner 55.

## TODOs

- [x] `0_collect.sh`: rsync
  - [ ] maybe: print progress similar to the `2_import.r` script, but maybe `rsync -i` is cool enough
  - optional: only convert files, that are unique to a new collection
    ! maybe this is easier done in `2_import.r` (parts of it are already prepared since 2024-2-17)
- [x] `1_convert.sh`: FIT to json
  - [ ] print progress similar to the `2_import.r` script
- [ ] `2_import.{sh|r}` .. *WIP*
  - store time series data of every category (monitor, sleep, etc) in respective R data.frames
  - single file per category (.rda)
  - backup as .csv
- [ ] `3_visualize.{sh|r}`
  - visualize time series of aggregated activities (all categories)
- [ ] wrapper script to ease the process of all tasks above and prevent forgetting about starting
  scripts every time in the correct order
