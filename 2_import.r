require(rjson)
require(data.table)

# load configuration
rc <- read.csv(file = "~/.faradorc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
rc_table <- data.table(rc, key = "key")
farado_data <- rc_table["farado_data"]$value
garmin_tz <- rc_table["garmin_tz"]$value

# get current date and time in UTC and the timezone of the garmin device
now <- as.POSIXct(Sys.time(), tz = "UTC")
today_utc <- strftime(now, "%Y-%m-%d", tz = "UTC")
today <- strftime(now, "%Y-%m-%d", tz = garmin_tz)

farado_data_collections <- list.dirs(
    path = paste0(farado_data),
    recursive = FALSE,
    full.names = FALSE
)

# todo
# - [x] duplicated entries have to be prevented (list of imported files does not need to be saved)
#   ! every new collection can have a subset of previuos collections (0_collect.sh and 1_convert.sh
#     don't look at duplicated files)
# ! [ ] remove all collection-dates from dir-list "farado_data_collections" if data-entries with
#   same collection-date already exists in imported data
# ? what if I want to import data in between that was left out?
#   - [x] 2_import.r will look at all collections every time, missed collections will be picked up
#     (this is useful, when 1_convert.sh was forgotten after 0_collect.sh, but started at a later
#     date)
#     ! note that if 0_collect.sh was not started every week, garmin data could be lost forever
#   - [ ] save the collection-date with every data-entry
#   - [ ] forced import, if a specific collection-date was given as a parameter,
#     even if already imported (this is useful when a collection was only partially imported
#     due to an interruption)
#     - delete every data-entry with that collection-date before re-import
#     - sort data.frame after re-import
#     ! note that this will still only re-import entries that are unique to a collection, because
#       every new collection can have a subset of previuos collections (and therefore older entries
#       present in a new collection can have a date from a previous collection).
#       - this will be implemented this way by design
# - [ ] merge data-entries by identical timestamp to remove NAs and keep file-size managable
# - optional: forced complete re-import
#   - easily done by checking, if the rda-files are present (a user could move existing files
#     and trigger a full re-import this way).

collection_count <- length(farado_data_collections)
cat("importing files from", collection_count, "collections\n")

times <- c()
stress_levels <- c()
respiration_rates <- c()
intensities <- c()
heart_rates <- c()

# monitor collections
i <- 0
last_time <- NA
all_monitor_files <- c()
for (collection in farado_data_collections) {
    monitor_files <- list.files(
        path = paste0(farado_data, collection, "/monitor"),
        full.names = FALSE
    )

    # remove duplicate files from the list
    monitor_files <- monitor_files[! monitor_files %in% all_monitor_files]
    file_count <- length(monitor_files)
    all_monitor_files <- c(all_monitor_files, monitor_files)

    i_file <- 1
    monitor_file_paths <- paste0(farado_data, collection, "/monitor/", monitor_files)
    for (json_file in monitor_file_paths) {
        #cat("import", json_file, "\n")
        json_parsed <- fromJSON(file = json_file)

        records <- json_parsed$file[[1]]$records
        for (record in records) {
            if (exists("stress_level", where = record)) {
                if (record$stress_level$stress_level_value > 0) {
                    last_time <- as.POSIXct(
                        record$stress_level$stress_level_time, tz = garmin_tz,
                        format = "%Y-%m-%dT%H:%M:%S"
                    )
                    times <- c(times, last_time)
                    stress_levels <- c(stress_levels, record$stress_level$stress_level_value)
                    respiration_rates <- c(respiration_rates, NA)
                    intensities <- c(intensities, NA)
                    heart_rates <- c(heart_rates, NA)
                }
            }
            else if (exists("respiration_rate", where = record)) {
                if (record$respiration_rate$respiration_rate_value > 0) {
                    last_time <- as.POSIXct(
                        record$respiration_rate$timestamp, tz = garmin_tz,
                        format = "%Y-%m-%dT%H:%M:%S"
                    )
                    times <- c(times, last_time)
                    respiration_rates <- c(
                        respiration_rates, record$respiration_rate$respiration_rate_value)
                    stress_levels <- c(stress_levels, NA)
                    intensities <- c(intensities, NA)
                    heart_rates <- c(heart_rates, NA)
                }
            }
            else if (exists("monitoring", where = record)) {
                if (exists("current_activity_type_intensity", where = record$monitoring)) {
                    if (exists("timestamp", where = record$monitoring)) {
                        last_time <- as.POSIXct(
                            record$monitoring$timestamp, tz = garmin_tz,
                            format = "%Y-%m-%dT%H:%M:%S"
                        )
                        times <- c(times, last_time)
                        intensities <- c(
                            intensities, record$monitoring$current_activity_type_intensity)
                        stress_levels <- c(stress_levels, NA)
                        respiration_rates <- c(respiration_rates, NA)
                        heart_rates <- c(heart_rates, NA)
                    }
                }
                else if (exists("heart_rate", where = record$monitoring)) {
                    if (!is.na(last_time) && record$monitoring$heart_rate > 0) {
                        times <- c(times, last_time)
                        heart_rates <- c(heart_rates, record$monitoring$heart_rate)
                        stress_levels <- c(stress_levels, NA)
                        respiration_rates <- c(respiration_rates, NA)
                        intensities <- c(intensities, NA)
                    }
                }
            }
        }

        # cat import progress
        collection_progress <- 100 / collection_count * i
        file_progress <- (100 / file_count * i_file) / collection_count
        progress <- round(collection_progress + file_progress, digits = 0)
        cat("\rmonitor:", paste0(progress, "%"))
        i_file <- i_file + 1
    }

    i <- i + 1
}

monitor <- data.frame(
    time = as.POSIXct(times, tz = garmin_tz),
    stress_level = stress_levels,
    respiration_rate = respiration_rates,
    intensity = intensities,
    heart_rate = heart_rates
)
cat(",", collection_count, "collections imported.\n")


